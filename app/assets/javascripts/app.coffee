carzone = angular.module('carzone', ['ngRoute', 'templates', 'ui.bootstrap', 'angular-loading-bar', 'ngSanitize'])

carzone.config(['$routeProvider', ($routeProvider) ->
  # Route for '/post'
  $routeProvider.when('/post/:postId', { templateUrl: '../assets/mainPost.html', controller: 'PostCtrl' } )

  # Default
  $routeProvider.otherwise({ templateUrl: 'mainIndex.html', controller: 'IndexCtrl' } )

])
