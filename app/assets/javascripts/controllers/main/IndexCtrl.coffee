@IndexCtrl = ($scope, $location, $http) ->

  $scope.data = 
    info: [{name: 'Loading posts...'}]
  loadPosts = (params = null) ->
    $http.get('./cars.json',
      params: params
    ).success( (data) ->
      $scope.data.cars = data
    ).error( ->
    )
  clearform = ->
    $scope.car = ''
  $scope.set_city = ->
    $scope.submitted = true
    city.value = cityselect.value

  loadPosts()

  $scope.viewPost = (postId) ->
    $location.url('/post/'+postId)

  $scope.$watchCollection "car", (newValue, oldValue) ->
    # Ignore initial setup.
    return  if newValue is oldValue
    loadPosts(options($scope))

  options = (scope) ->
    option = []
    if scope.car.brand
      option['brand'] = $scope.car.brand.id
    if scope.car.model
      option['model'] = $scope.car.model.id
    if scope.car.color
      option['color'] = $scope.car.color
    if scope.car.engine
      option['engine'] = $scope.car.engine
    if scope.car.transmission
      option['transmission'] = $scope.car.transmission
    option
@CarouselDemoCtrl = ($scope, $http) ->
  $scope.myInterval = 5000
  slides = $scope.slides = []
  $http(
    method: "Get"
    url: "/sliders"
  ).success((data, status, headers, config) ->
    $scope.slides = data
  ).error (data, status, headers, config) ->
    $scope.message = "Unexpected Error"
