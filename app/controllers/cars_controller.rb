class CarsController < ApplicationController
  respond_to :json
  def index
    @cars = Car.joins(car_model: :brand).where(conditions)
    #respond_with( @cars, include: {car_model: {include: :cars}}, :root => true)
    respond_with(@cars, root: true)
  end

  private

  def conditions
    c = {}
    c.merge!(brands:     {id: params[:brand]})                  if params[:brand]
    c.merge!(car_models: {id: params[:model]})                  if params[:model]
    if params[:color] || params[:engine] || params[:transmission]
      c[:cars] = {}
      c[:cars][:color]= params[:color]               if params[:color]
      c[:cars][:engine]= params[:engine]             if params[:engine]
      c[:cars][:transmission] = params[:transmission] if params[:transmission]
    end
    c
  end
end
