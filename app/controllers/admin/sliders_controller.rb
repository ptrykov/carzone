class Admin::SlidersController < Admin::AdminController

  before_action :set_slider, only: [:show, :edit, :update, :destroy]

  def index
    @sliders = Slider.all
  end

  def new
    @slider = Slider.new
  end

  def create
    @slider = Slider.new(slider_params)
    if @slider.save
      redirect_to action: :index
    else
      render "new"
    end
  end

  def show
  
  end

  def edit

  end

  def update
    if @slider.update(slider_params)
      redirect_to action: :index
    else
      render :edit
    end
  end

  def destroy
    @slider.destroy
    redirect_to action: 'index'
  end

  private

  def set_slider
    @slider = Slider.find(params[:id])
  end

  def slider_params
    params.require(:slider).permit!
  end
end
