class Admin::RequestsController < Admin::AdminController
  before_action :set_request, only: [:edit, :update, :destroy]
  
  def index
    @q = Request.search(params[:q])
    @requests = @q.result.order(:id).page params[:page]
    respond_to do |format|
      format.html
      format.xls { send_data(@requests.to_xls) }
    end
  end

  def edit

  end

  def update
    if @request.update(request_params)
      redirect_to action: :index
    else
      render :edit
    end
  end

  def destroy
    @request.destroy
    redirect_to action: :index
  end

  private

  def set_request
    @request = Request.find(params[:id])
  end

  def request_params
    params.require(:request).permit!
  end
end
