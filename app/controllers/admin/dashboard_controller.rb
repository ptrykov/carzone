class Admin::DashboardController < Admin::AdminController
  before_action :set_request, only:[:edit, :update, :destroy]
  
  def index
    @q = Request.search(params[:q])
    @requests = @q.result.order(:id).page params[:page]
  end

  def edit
    #redirect_to action: 'index'
  end

  def update
    if @query.update(dashboard_params)
      redirect_to action: :index
    else
      render :edit
    end
  end

  def destroy
    @request.destroy
    redirect_to action: 'index'
  end

  private

  def set_request
    @request = Request.find(params[:id])
  end

  def dashboard_params
    params.require(:request).permit!
  end
end
