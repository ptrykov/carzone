class SlidersController < ApplicationController
  respond_to :json
  def index
    @sliders = Slider.all
    respond_with @sliders
  end
end
