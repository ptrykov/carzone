class PagesController < ApplicationController

  PAGES = [:agreement, :privacy, :help, :contact]

  def show
    @page = params[:id].to_sym rescue nil

    redirect_to root_url and return unless PagesController::PAGES.include?(@page)

    #@contact = Contact.new(params[:contact]) if @page == :contact

    render :file => "pages/#{@page}"
  end
end
