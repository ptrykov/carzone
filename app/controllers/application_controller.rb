# encoding: utf-8
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception
  before_action :all_cities
  
  def all_cities
    @cities = City.all.map(&:name)
    @current_city = @cities.first
    city = Ipgeobase.lookup(request.remote_ip).city rescue nil
    if city.present?
      @cities.push(city).uniq!
      @current_city = city
    end
  end
end
