#encoding: utf-8
class MainController < ApplicationController
  def index
  end
  def create
    notify_admin
    Request.create(main_params)
    redirect_to(root_path, notice: 'Ваша заявка принята. Спасибо!')
  end

  private
  
  def notify_admin
    params[:model] = CarModel.find_by_id(params[:model]).try(:name)
    params['brand'] = Brand.find_by_id(params['brand']).try(:name)
    FormNotifier.request_email(params).deliver
  end

  def main_params
    params.permit(:name, :surname, :city, :email, :brand, :model, :color, :engine, :transmission, :flexible)
  end
end
