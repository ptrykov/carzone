#encoding: utf-8
class FormNotifierPreview < ActionMailer::Preview
  def request_email_test
    params = {"brand"=>"1", "model"=>"1", "color"=>"Белый металлик", "engine"=>"1.2 TFSI", "transmission"=>"Mechanik", "name"=>"Трыков", "surname"=>"Павел", "email"=>"ptrykov@gmail.com", "city"=>"Москва"}
    params['model'] = CarModel.find_by_id(params['model']).try(:name)
    params['brand'] = Brand.find_by_id(params['brand']).try(:name)
    FormNotifier.request_email(params)
  end
end
