#encoding: utf-8
class FormNotifier < ActionMailer::Base
  default from: 'andrewvl22@gmail.com', reply_to: 'andrewvl22@gmail.com'

  def request_email(form)
    @form = form
    mail to: 'andrewvl22@gmail.com', subject: 'Новая заявка'
  end
end
