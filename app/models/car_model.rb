class CarModel < ActiveRecord::Base
  belongs_to :brand
  has_many :cars
end
