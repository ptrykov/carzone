class Car < ActiveRecord::Base
  belongs_to :car_model
  has_one :brand, through: :car_model
end
