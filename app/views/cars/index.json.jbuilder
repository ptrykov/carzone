json.colors do
  json.array! @cars.map(&:color).uniq 
end

json.engine do
  json.array! @cars.map(&:engine).uniq 
end

json.transmission do
  json.array! @cars.map(&:transmission).uniq
end

json.car_models do
  json.array! @cars.map(&:car_model).uniq do |car_model|
    json.(car_model, :id, :name)
  end
end
json.brands do
  json.array! @cars.map(&:brand).uniq do |brand|
    json.(brand, :id, :name)
  end
end
