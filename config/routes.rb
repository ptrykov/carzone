Rails.application.routes.draw do
  devise_for :users
  #get 'main/index'
  resource :main, only: [:index, :create], controller: 'main'
  resources :pages, :only => :show
  root 'main#index'

  resources :cars, defaults: {format: :json}
  resources :sliders, defaults: {format: :json}

  namespace :dynamic_select do
    get ':brand_id/car_models', to: 'car_models#index', as: 'car_models'
    get ':car_model_id/colors', to: 'car_models#color', as: 'car_colors'
  end

  namespace :admin do
    root 'requests#index'
    resources :requests
    resources :sliders
    resources :brands do
    end
    resources :car_models
    resources :cars

  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
