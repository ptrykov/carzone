class AddPictureToSlider < ActiveRecord::Migration
  def self.up
    add_attachment :sliders, :picture
  end

  def self.down
    remove_attachment :sliders, :picture
  end
end
