class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :name
      t.string :code
      t.string :color
      t.string :engine
      t.string :transmission
      t.references :car_model, index: true

      t.timestamps
    end
  end
end
