class CreateSliders < ActiveRecord::Migration
  def change
    create_table :sliders do |t|
      t.string :name
      t.string :info
      t.string :title

      t.timestamps
    end
  end
end
