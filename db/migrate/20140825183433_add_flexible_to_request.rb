class AddFlexibleToRequest < ActiveRecord::Migration
  def change
    add_column :requests, :flexible, :boolean, default: false
  end
end
