class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.string :name
      t.string :surname
      t.string :email
      t.string :brand
      t.string :model
      t.string :color
      t.string :engine
      t.string :transmission
      t.string :city

      t.timestamps
    end
  end
end
