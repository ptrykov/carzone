Brand.seed(:id,
          { id: 1, name: 'Audi', code: 'audi'},
          { id: 2, name: 'Bmw' , code: 'bmw'}
          )
audi = Brand.find 1
bmw  = Brand.find 2

CarModel.seed(:id,
             { id: 1, name: 'A3', code: 'a3', brand: audi},
             { id: 2, name: 'A4', code: 'a4', brand: audi},
             { id: 3, name: 'A6', code: 'a6', brand: audi},
             { id: 4, name: '3',  code: '3',  brand: bmw},
             { id: 5, name: '5',  code: '5',  brand: bmw}
             )

a3 = CarModel.find(1)
a4 = CarModel.find(2)
a6 = CarModel.find(3)
bmw3 = CarModel.find(4)
bmw5 = CarModel.find(5)

Car.seed(:id,
          {name: "Audi A3", color: 'Белый неметаллик', 			engine: '1.2 TFSI',              transmission: '6-ст. механическая',    code:  "audi_A3",      car_model: a3},
          {name: "Audi A3", color: 'Красный неметаллик',		engine: '1.2 TFSI',              transmission: 'S tronic',              code:  "audi_A3",      car_model: a3},
          {name: "Audi A3", color: 'Черный неметаллик',			engine: '1.4 TFSI',              transmission: '6-ст. механическая',    code:  "audi_A3",      car_model: a3},
          {name: "Audi A3", color: 'Белый металлик',				engine: '1.4 TFSI',              transmission: 'S tronic',              code:  "audi_A3",      car_model: a3},
          {name: "Audi A3", color: 'Коричневый металлик',		engine: '1.8 TFSI',              transmission: '6-ст. механическая',    code:  "audi_A3",      car_model: a3},
          {name: "Audi A3", color: 'Красный металлик',			engine: '1.8 TFSI',              transmission: 'S tronic',              code:  "audi_A3",      car_model: a3},
          {name: "Audi A3", color: 'Серебристый металлик',	engine: '1.8 TFSI',              transmission: 'S tronic',              code:  "audi_A3",      car_model: a3},
          {name: "Audi A3", color: 'Серый металлик', 				engine: '2.0 TDI' ,              transmission: 'S tronic',              code:  "audi_A3",      car_model: a3},
          {name: "Audi A4", color: 'Синий металлик',				engine: '1.8 TFSI',              transmission: '6-ст. механическая',    code:  "audi_A4",      car_model: a4},
          {name: "Audi A4", color: 'Красный перламутр',			engine: '1.8 TFSI',              transmission: 'Multitronic',           code:  "audi_A4",      car_model: a4},
          {name: "Audi A4", color: 'Серый перламутр',				engine: '1.8 TFSI',              transmission: '6-ст. механическая',    code:  "audi_A4",      car_model: a4},
          {name: "Audi A4", color: 'Черный перламутр',      engine: '1.8 TFSI',              transmission: 'Multitronic',           code:  "audi_A4",      car_model: a4},
          {name: "Audi A4", color: 'Черный перламутр',      engine: '1.8 TFSI',              transmission: '6-ст. механическая',    code:  "audi_A4",      car_model: a4},
          {name: "Audi A4", color: 'Черный перламутр',      engine: '2.0 TFSI',              transmission: '6-ст. механическая',    code:  "audi_A4",      car_model: a4},
          {name: "Audi A4", color: 'Черный перламутр',      engine: '2.0 TFSI',              transmission: 'Multitronic',           code:  "audi_A4",      car_model: a4},
          {name: "Audi A4", color: 'Черный перламутр',      engine: '2.0 TFSI',              transmission: '6-ст. механическая',    code:  "audi_A4",      car_model: a4},
          {name: "Audi A4", color: 'Черный перламутр',      engine: '2.0 TFSI',              transmission: 'S tronic',              code:  "audi_A4",      car_model: a4},
          {name: "Audi A4", color: 'Черный перламутр',      engine: '3.0 TFSI',              transmission: 'S tronic',              code:  "audi_A4",      car_model: a4},
          {name: "Audi A6", color: 'Белый неметаллик', 		  engine: '2.0 TFSI',              transmission: '6-ст. механическая',    code:  "audi_A6",      car_model: a6},
          {name: "Audi A6", color: 'Черный неметаллик',			engine: '2.0 TFSI',              transmission: 'Multitronic',           code:  "audi_A6",      car_model: a6},
          {name: "Audi A6", color: 'Белый металлик',				engine: '2.8 FSI' ,              transmission: '6-ст. механическая',    code:  "audi_A6",      car_model: a6},
          {name: "Audi A6", color: 'Коричневый металлик',		engine: '2.8 FSI' ,              transmission: 'Multitronic',           code:  "audi_A6",      car_model: a6},
          {name: "Audi A6", color: 'Серый металлик', 				engine: '2.8 FSI' ,              transmission: 'S tronic',              code:  "audi_A6",      car_model: a6},
          {name: "Audi A6", color: 'Серебристый металлик',	engine: '3.0 TFSI',              transmission: 'S tronic',              code:  "audi_A6",      car_model: a6},
          {name: "Audi A6", color: 'Черный металлик',				engine: '2.0 TDI' ,              transmission: 'Multitronic',           code:  "audi_A6",      car_model: a6},
          {name: "Audi A6", color: 'Синий металлик',			  engine: '3.0 TDI' ,              transmission: 'S tronic',              code:  "audi_A6",      car_model: a6},
          {name: "BMW 3",   color: 'Белый',						      engine: '316i (1,598 cм3)',       transmission: 'Автомат',               code:    "bmw_3",      car_model: bmw3},
          {name: "BMW 3",   color: 'Черный',						    engine: '320d (1,995 см3)',       transmission: 'Автомат',               code:    "bmw_3",      car_model: bmw3},
          {name: "BMW 3",   color: 'Белый минерал',				  engine: '320i SE (1,997 см3)',    transmission: 'Автомат',               code:    "bmw_3",      car_model: bmw3},
          {name: "BMW 3",   color: 'Красный',						    engine: '328i (1,997 см3)',       transmission: 'Автомат',               code:    "bmw_3",      car_model: bmw3},
          {name: "BMW 3",   color: 'Серебристый',					  engine: '335i (2,979 см3)',       transmission: 'Автомат',               code:    "bmw_3",      car_model: bmw3},
          {name: "BMW 3",   color: 'Синий',						      engine: '335i (2,979 см3)',       transmission: 'Автомат',               code:    "bmw_3",      car_model: bmw3},
          {name: "BMW 3",   color: 'Серый',                 engine: '335i (2,979 см3)',       transmission: 'Автомат',               code:    "bmw_3",      car_model: bmw3},
          {name: "BMW 5",   color: 'Черный',                engine: '520d (1,995 см3)',       transmission: 'Автомат',               code:    "bmw_5",      car_model: bmw5},
          {name: "BMW 5",   color: 'Белый',                 engine: '528i (1,997 см3)',       transmission: 'Автомат',               code:    "bmw_5",      car_model: bmw5},
          {name: "BMW 5",   color: 'Серебристый',					  engine: '530d (2,993 см3)',       transmission: 'Автомат',               code:    "bmw_5",      car_model: bmw5},
          {name: "BMW 5",   color: 'Синий',                 engine: '535i (2,979 см3)',       transmission: 'Автомат',               code:    "bmw_5",      car_model: bmw5},
          {name: "BMW 5",   color: 'Темно-синий',           engine: '550i (4,395 см3)',       transmission: 'Автомат',               code:    "bmw_5",      car_model: bmw5},
          {name: "BMW 5",   color: 'Черный',                engine: 'M550d (2,993 см3)',      transmission: 'Автомат',               code:    "bmw_5",      car_model: bmw5},
          {name: "BMW 5",   color: 'Серый',                 engine: 'M550d (2,993 см3)',      transmission: 'Автомат',               code:    "bmw_5",      car_model: bmw5}
        )
