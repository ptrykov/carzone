# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :car do
    name "MyString"
    code "MyString"
    color "MyString"
    engine "MyString"
    transmission "MyString"
    car_model nil
  end
end
