# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :slide do
    name "MyString"
    info "MyString"
    title "MyString"
  end
end
