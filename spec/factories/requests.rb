# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :request do
    name "MyString"
    surname "MyString"
    email "MyString"
    brand "MyString"
    model "MyString"
    color "MyString"
    engine "MyString"
    transmission "MyString"
    city "MyString"
  end
end
