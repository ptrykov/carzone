require 'rails_helper'

RSpec.describe "admin/cars/new", :type => :view do
  before(:each) do
    assign(:admin_car, Admin::Car.new())
  end

  it "renders new admin_car form" do
    render

    assert_select "form[action=?][method=?]", admin_cars_path, "post" do
    end
  end
end
