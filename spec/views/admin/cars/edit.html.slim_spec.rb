require 'rails_helper'

RSpec.describe "admin/cars/edit", :type => :view do
  before(:each) do
    @admin_car = assign(:admin_car, Admin::Car.create!())
  end

  it "renders the edit admin_car form" do
    render

    assert_select "form[action=?][method=?]", admin_car_path(@admin_car), "post" do
    end
  end
end
