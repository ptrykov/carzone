require 'rails_helper'

RSpec.describe "admin/brands/edit", :type => :view do
  before(:each) do
    @admin_brand = assign(:admin_brand, Admin::Brand.create!())
  end

  it "renders the edit admin_brand form" do
    render

    assert_select "form[action=?][method=?]", admin_brand_path(@admin_brand), "post" do
    end
  end
end
