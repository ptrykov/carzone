require 'rails_helper'

RSpec.describe "admin/brands/new", :type => :view do
  before(:each) do
    assign(:admin_brand, Admin::Brand.new())
  end

  it "renders new admin_brand form" do
    render

    assert_select "form[action=?][method=?]", admin_brands_path, "post" do
    end
  end
end
